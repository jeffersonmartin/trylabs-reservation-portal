<span class="small">
    Powered by <a target="_blank" href="https://www.trylabs.cloud">TryLabs</a>, a managed service from <a target="_blank" href="https://www.cloudstration.com">Cloudstration Inc.</a><br />
    <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>
</span>
