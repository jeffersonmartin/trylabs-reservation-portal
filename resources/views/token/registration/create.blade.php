@extends('_layouts.core')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-token-laptop-purple-small.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-10 col-md-7 col-lg-7 col-xl-7 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden ribbon ribbon-info">

                    {{--
                    <div class="block-content bg-primary-lighter text-center py-3">
                        <a href="{{ route('auth.login.create') }}">Already have an account? <span class="font-w700">Sign in</span> to access your account.</a>
                    </div>
                    --}}

                    <div class="block-content block-content-full px-lg-5 px-xl-6 pt-5 pb-5 bg-body-light border-bottom">

                        <div class="ribbon-box">
                            a1b2c3d4
                        </div>

                        <div class="d-flex">

                            <div class="flex-00-auto mr-4 mt-1">
                                <i class="si si-screen-desktop fa-4x text-gray"></i>
                            </div>
                            <div class="flex-fill">
                                <span class="font-size-h2 font-w600 text-center">Lab Access Token Title</span><br />
                                <span class="text-secondary">Created by LAB AUTHOR NAME OR COMPANY</span><br />
                            </div>
                        </div>

                    </div>
                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-5 bg-white">

                        @if($errors->any())
                            <div class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    {{ $error }}<br />
                                @endforeach
                            </div>
                        @else
                            <div class="alert alert-info mb-4">
                                Please complete the registration form below to access the lab. Your contact information will be shared with the Lab Creator for usage tracking purposes.
                            </div>
                        @endif

                        <form class="" action="{{-- route('token.register.store') --}}" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control form-control-lg" id="first_name" name="first_name" placeholder="" value="{{ $request->old('first_name') }}" maxlength=30>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control form-control-lg" id="last_name" name="last_name" placeholder="" value="{{ $request->old('last_name') }}"  maxlength=30>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="organization_name">Organization Name <span class="small text-secondary">(optional)</span></label>
                                        <input type="text" class="form-control form-control-lg" id="organization_name" name="organization_name" placeholder=""  value="{{ $request->old('organization_name') }}"maxlength=50>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="" maxlength=100 value="{{ $request->old('email') }}">
                            </div>

                            {{--
                            <div class="form-group">
                                <label for="job_title">How Do You Spend Your Time?</label>
                                <select class="form-control form-control-lg" name="job_title" id="job_title">
                                    <option {!! $request->old('job_title') == "Undisclosed Supernatural Powers and Abilities" ? 'selected="selected"' : '' !!} value="Undisclosed Supernatural Powers and Abilities">Undisclosed Supernatural Powers and Abilities</option>
                                    <optgroup label="Casual Engineer">
                                        <option {!! $request->old('job_title') == "Linux / Open Source Hacker" ? 'selected="selected"' : '' !!} value="Linux / Open Source Hacker">Linux / Open Source Hacker</option>
                                        <option {!! $request->old('job_title') == "Home Lab / Garage Enthusiast" ? 'selected="selected"' : '' !!} value="Home Lab / Garage Enthusiast">Home Lab / Garage Enthusiast</option>
                                        <option {!! $request->old('job_title') == "Video Game Champion" ? 'selected="selected"' : '' !!} value="Video Game Champion">Video Game Champion</option>
                                    </optgroup>
                                    <optgroup label="Professional Engineer">
                                        <option {!! $request->old('job_title') == "Technical Manager or Team Lead" ? 'selected="selected"' : '' !!} value="Technical Manager or Team Lead">Technical Manager or Team Lead</option>
                                        <option {!! $request->old('job_title') == "Hardware / Network Engineer" ? 'selected="selected"' : '' !!} value="Hardware / Network Engineer">Hardware / Network Engineer</option>
                                        <option {!! $request->old('job_title') == "DevOps / Server Administrator" ? 'selected="selected"' : '' !!} value="DevOps / Server Administrator">DevOps / Server Administrator</option>
                                        <option {!! $request->old('job_title') == "Datacenter / Site Reliability Engineer" ? 'selected="selected"' : '' !!} value="Datacenter / Site Reliability Engineer">Datacenter / Site Reliability Engineer</option>
                                        <option {!! $request->old('job_title') == "Software Developer / Engineer" ? 'selected="selected"' : '' !!} value="Software Developer / Engineer">Software Developer / Engineer</option>
                                        <option {!! $request->old('job_title') == "IT Department Staff" ? 'selected="selected"' : '' !!} value="IT Department Staff">IT Department Staff</option>
                                        <option {!! $request->old('job_title') == "Product Technical Support" ? 'selected="selected"' : '' !!} value="IT Department Staff">Product Technical Support</option>
                                        <option {!! $request->old('job_title') == "IT Consultant / Professional Services" ? 'selected="selected"' : '' !!} value="IT Department Staff">IT Consultant / Professional Services</option>
                                        <option {!! $request->old('job_title') == "Other Cool Technical Stuff You Haven't Heard Of" ? 'selected="selected"' : '' !!} value="Other Cool Technical Stuff You Haven't Heard Of">Other Cool Technical Stuff You Haven't Heard Of</option>
                                    </optgroup>
                                    <optgroup label="Sales and Marketing">
                                        <option {!! $request->old('job_title') == "Sales Manager / Director" ? 'selected="selected"' : '' !!} value="Sales Manager / Director">Sales Manager / Director</option>
                                        <option {!! $request->old('job_title') == "Sales Representative / Associate" ? 'selected="selected"' : '' !!} value="Sales Representative / Associate">Sales Representative / Associate</option>
                                        <option {!! $request->old('job_title') == "Sales Engineer / Solutions Architect" ? 'selected="selected"' : '' !!} value="Sales Engineer / Solutions Architect">Sales Engineer / Solutions Architect</option>
                                        <option {!! $request->old('job_title') == "Sales Enablement Team" ? 'selected="selected"' : '' !!} value="Sales Enablement Team">Sales Enablement Team</option>
                                        <option {!! $request->old('job_title') == "Marketing Team" ? 'selected="selected"' : '' !!} value="Marketing Team">Marketing Team</option>
                                        <option {!! $request->old('job_title') == "Training Team" ? 'selected="selected"' : '' !!} value="Training Team">Training Team</option>
                                        <option {!! $request->old('job_title') == "Partner Enablement Team" ? 'selected="selected"' : '' !!} value="Partner Enablement Team">Partner Enablement Team</option>
                                    </optgroup>
                                    <optgroup label="Management">
                                        <option {!! $request->old('job_title') == "Founder / C-Level" ? 'selected="selected"' : '' !!} value="Founder / C-Level">Founder / C-Level</option>
                                        <option {!! $request->old('job_title') == "VP / Director Business Management" ? 'selected="selected"' : '' !!} value="VP / Director Business Management">VP / Director Business Management</option>
                                        <option {!! $request->old('job_title') == "Project / Program Manager" ? 'selected="selected"' : '' !!} value="Project / Program Manager">Project / Program Manager</option>
                                        <option {!! $request->old('job_title') == "Non-Technical Manager or Team Lead" ? 'selected="selected"' : '' !!} value="Non-Technical Manager or Team Lead">Non-Technical Manager or Team Lead</option>
                                    </optgroup>
                                    <optgroup label="Academic">
                                        <option {!! $request->old('job_title') == "Instructor / Professor / Teacher" ? 'selected="selected"' : '' !!} value="Instructor / Professor / Teacher">Instructor / Professor / Teacher</option>
                                        <option {!! $request->old('job_title') == "College Student" ? 'selected="selected"' : '' !!} value="College Student">College Student</option>
                                        <option {!! $request->old('job_title') == "High School Student" ? 'selected="selected"' : '' !!} value="High School Student">High School Student</option>
                                        <option {!! $request->old('job_title') == "Technical Certifications" ? 'selected="selected"' : '' !!} value="Technical Certifications">Technical Certifications</option>
                                        <option {!! $request->old('job_title') == "Self-Taught Student" ? 'selected="selected"' : '' !!} value="Self-Taught Student">Self-Taught Student</option>
                                    </optgroup>
                                    <option {!! $request->old('job_title') == "Other Magical Abilities" ? 'selected="selected"' : '' !!} value="Other Magical Abilities">Other Magical Abilities</option>
                                </select>
                            </div>
                            --}}

                            <!--
                            <div class="form-group text-center">
                                <a class="font-w600 font-size-sm" href="#" data-toggle="modal" data-target="#modal-terms">Terms &amp; Conditions</a>
                                <div class="custom-control custom-checkbox custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="signup-terms" name="signup-terms">
                                    <label class="custom-control-label" for="signup-terms">I agree</label>
                                </div>
                            </div>
                            -->

                            <div class="row">
                                <div class="col-12 mt-3">
                                    <ul class="list-group push">
                                        <li class="list-group-item">
                                            <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                                <input type="checkbox" class="custom-control-input" id="flag_privacy_accepted" name="flag_privacy_accepted" {!! $request->old('flag_privacy_accepted') == true ? 'checked="checked"' : '' !!}>
                                                <label class="custom-control-label" for="flag_privacy_accepted">
                                                    I accept the <a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>.
                                                    <div class="small text-secondary mt-3">
                                                        We use cookies and collect your contact information for billing purposes. Your contact information (name, organization and email address) will be shared with the Lab Creator (LAB AUTHOR NAME OR COMPANY) for usage tracking purposes. By registering for a lab that is created by and paid for by the Lab Creator, you are consenting to their privacy policy, usage data tracking practices, and may be subscribed to their mailing list.<br />
                                                        <br />
                                                        Any virtual machine operating system files, software licenses, text content and multimedia content ("service data") are kept private by default, however there is always a risk of misconfiguration in a lab environment that could compromise your data privacy or security.<br />
                                                        <br />
                                                        <span class="font-w700">DO NOT UPLOAD OR USE ANY SENSITIVE DATA IN A LAB ENVIRONMENT.</span>
                                                    </div>
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="text-center">
                                <a class="btn btn-lg btn-outline-secondary mr-2" href="#">Cancel</a>
                                <button type="submit" class="btn btn-lg btn-success">
                                    Register and Start Lab<i class="fa fa-chevron-right ml-2"></i>
                                </button>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    <div class="block-content bg-body-light border-top py-3">
                        <div class="row">
                            <div class="col-7">
                                <span class="small">
                                    Powered by <a target="_blank" href="https://www.trylabs.io">TryLabs.io</a><br />
                                    <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>
                                </span>
                            </div>
                            <div class="col-5 text-right">
                                <a class="btn btn-outline-info mt-1" target="_blank" href="{{-- route('support.dashboard.index') --}}"><i class="fa fa-question-circle mr-2"></i>Contact the Lab Creator</a>
                            </div>
                        </div>
                    </div>
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-2012-2022.js"></script>
    <script>
        var timezone = moment.tz.guess();
        $('#timezone').val(timezone);
    </script>
@endsection
