@extends('_layouts.core')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-token-laptop-purple-small.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-10 col-md-7 col-lg-7 col-xl-7 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden ribbon ribbon-info">

                    {{--
                    <div class="block-content bg-primary-lighter text-center py-3">
                        <a href="#">Don't have an account? <span class="font-w700">Create an account</span> to get started for free.</a>
                    </div>
                    --}}

                    <div class="block-content block-content-full px-lg-5 px-xl-6 pt-5 pb-3 bg-body-light border-bottom">

                        <div class="ribbon-box">
                            {{ $token->data->short_id }}
                        </div>

                        <div class="d-flex mb-4">

                            <div class="flex-00-auto mr-4 mt-1">
                                <i class="si si-screen-desktop fa-4x text-gray"></i>
                            </div>
                            <div class="flex-fill">
                                <span class="font-size-h2 font-w600 text-center">{{ $token->data->name }}</span><br />

                                <span class="text-info">Your lab reservation will expire in <span class="font-w700">{{ $token->data->time_limit_minutes }} Minutes</span></span><br />
                                <br />
                            </div>
                        </div>

                        @if($token->data->external_notes)
                            <p class="pb-0">
                                {{ $token->data->external_notes }}
                            </p>
                        @endif

                    </div>
                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-5 bg-white">

                        @if($errors->any())
                            <div class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    {{ $error }}<br />
                                @endforeach
                            </div>
                        @endif

                        @if (session('alert'))
                            <div class="alert alert-danger">
                                {{ session('alert') }}
                            </div>
                        @endif

                        <form class="" action="{{ route('token.redeem', ['token_short_id' => $token->data->short_id]) }}" method="POST">
                            @csrf

                            <input type="hidden" name="tenant_reservation_token_id" id="{{ $token->data->id }}" />
                            <input type="hidden" name="timezone" id="timezone" />

                            @if($token->data->flags->flag_is_password_protected == 1)

                                <div class="alert alert-info text-center mb-5">
                                    <div class="form-group col-lg-6 col-md-8 col-sm-10 col-xs-12 offset-sm-1 offset-md-2 offset-lg-3 mt-1 mb-3">
                                        <label for="password" class="mt-2 mb-3">
                                            Lab Password<br />
                                            <span class="small">
                                                Please enter the lab access password that was <br />
                                                provided by the lab creator to launch this lab.
                                            </span>
                                        </label>
                                        <input type="password" class="form-control form-control-lg border-info" id="password" name="password" placeholder="" value="{{ $request->old('password') }}">
                                    </div>


                                </div>
                            @endif

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control form-control-lg" id="first_name" name="first_name" placeholder="" value="{{ $request->old('first_name') }}" maxlength=30>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control form-control-lg" id="last_name" name="last_name" placeholder="" value="{{ $request->old('last_name') }}"  maxlength=30>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="organization_name">Organization Name <span class="small text-secondary">(optional)</span></label>
                                        <input type="text" class="form-control form-control-lg" id="organization_name" name="organization_name" placeholder=""  value="{{ $request->old('organization_name') }}"maxlength=50>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="" maxlength=100 value="{{ $request->old('email') }}">
                            </div>

                            <div class="form-group text-center">
                                <div class="custom-control custom-checkbox custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="terms" name="flag_terms_accepted">
                                    <label class="custom-control-label" for="terms">I agree to the <a href="{{ route('legal.terms.show') }}" target="_blank">Terms of Service</a></label>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <div class="custom-control custom-checkbox custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="privacy" name="flag_privacy_accepted">
                                    <label class="custom-control-label" for="privacy">I agree to the <a href="{{ route('legal.privacy.show') }}" target="_blank">Privacy Policy</a></label>
                                </div>
                            </div>


                            <div class="pt-2 text-center">
                                <button type="button" class="btn btn-lg btn-success" readonly="readonly" v-b-tooltip.hover title="Lab redemptions are temporarily unavailable. Please check back later.">
                                    Launch Lab
                                </button>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    <div class="block-content bg-body-light border-top py-3">
                        <div class="row">
                            <div class="col-7">
                                @include('_partials.footer')
                            </div>
                            <div class="col-5 text-right">
                                <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:{{ $token->data->relationships->tenant_account->data->user_support_email}}?s=Question about TryLabs Token {{ $token->data->short_id }} - {{ $token->data->name }}"><i class="fa fa-question-circle mr-2"></i>Contact the Lab Creator</a>
                            </div>
                        </div>
                    </div>
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-2012-2022.js"></script>
    <script>
        var timezone = moment.tz.guess();
        $('#timezone').val(timezone);
    </script>
@endsection
