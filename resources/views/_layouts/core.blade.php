<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>TryLabs</title>

        <meta name="description" content="Simplifying virtual labs for training and software demos.">
        <meta name="author" content="Cloudstration">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Cloudstration Labs">
        <meta property="og:site_name" content="Cloudstration">
        <meta property="og:description" content="Simplifying virtual labs for training and software demos.">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Page refresh -->
        @yield('head_meta')

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')
        <link rel="stylesheet" id="css-main" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link rel="stylesheet" id="css-theme" href="{{ mix('css/dashmix.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fonts/icomoon.css') }}">
        <link rel="stylesheet" href="{{ mix('css/themes/xsmooth.css') }}">
        @yield('css_after')

        @yield('head_js')

        <!-- Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133688728-5"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-133688728-5');
        </script>

        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>
        <div id="app">
            <div id="page-container" class="">
                <!-- Main Container -->
                <main id="main-container">
                    @yield('content')
                </main><!-- END Main Container -->
            </div><!-- END Page Container -->
        </div>

        @yield('modals')

        @yield('js_before')

        <!-- JS Scripts -->
        <script src="{{ mix('js/dashmix.app.js') }}"></script>
        <script src="{{ mix('js/laravel.app.js') }}"></script>

        @yield('js_after')
    </body>
</html>
