@extends('_layouts.core')

@section('head_meta')
    <meta http-equiv="refresh" content="15" />
@endsection

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-token-laptop-purple-small.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-10 col-md-7 col-lg-7 col-xl-7 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden ribbon ribbon-info">

                    {{--
                    <div class="block-content bg-primary-lighter text-center py-3">
                        <a href="{{ route('auth.login.create') }}">Already have an account? <span class="font-w700">Sign in</span> to access your account.</a>
                    </div>
                    --}}

                    {{-- Reservation Heading based on Image Template --}}
                    @if($reservation->data->tenant_reservation_token_id == null)
                        <div class="block-content block-content-full px-lg-5 px-xl-6 pt-5 pb-3 bg-body-light border-bottom">

                            <div class="ribbon-box">
                                {{ $reservation->data->short_id }}
                            </div>

                            <div class="d-flex mb-4">

                                <div class="flex-00-auto mr-4 mt-1">
                                    <i class="si si-screen-desktop fa-4x text-gray"></i>
                                </div>
                                <div class="flex-fill">
                                    <span class="font-size-h2 font-w600 text-center">{{ $reservation->data->relationships->cloud_image_template->data->name }}</span><br />
                                    <span class="text-secondary">
                                        {!! $reservation->data->relationships->cloud_image_template->data->version ? 'Version '.$reservation->data->relationships->cloud_image_template->data->version : 'Created '.\Carbon\Carbon::parse($reservation->data->relationships->cloud_image_template->data->timestamps->created_at)->toDayDateTimeString().' UTC'; !!}
                                    </span>
                                    <br />
                                </div>
                            </div>

                            @if($reservation->data->relationships->cloud_image_template->data->description)
                                <p class="pb-0">
                                    {{ $reservation->data->relationships->cloud_image_template->data->description }}
                                </p>
                            @endif

                        </div>

                    {{-- Reservation Heading based on Reservation Token --}}
                    @else
                        <div class="block-content block-content-full px-lg-5 px-xl-6 pt-5 pb-3 bg-body-light border-bottom">

                            <div class="ribbon-box">
                                {{ $reservation->data->short_id }}
                            </div>

                            <div class="d-flex mb-4">

                                <div class="flex-00-auto mr-4 mt-1">
                                    <i class="si si-screen-desktop fa-4x text-gray"></i>
                                </div>
                                <div class="flex-fill">
                                    <span class="font-size-h2 font-w600 text-center">{{ $reservation->data->relationships->tenant_reservation_token->data->name }}</span><br />
                                    <span class="text-secondary">
                                        {!! $reservation->data->relationships->tenant_reservation_token->data->version ? 'Version '.$reservation->data->relationships->cloud_image_template->data->version : '' !!}
                                    </span>
                                    <br />
                                </div>
                            </div>

                            @if($reservation->data->relationships->tenant_reservation_token->data->external_notes)
                                <p class="pb-0">
                                    {{ $reservation->data->relationships->tenant_reservation_token->data->external_notes }}
                                </p>
                            @endif

                        </div>
                    @endif
                    {{-- End of Reservation Heading --}}

                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-5 bg-white">

                        @if($errors->any())
                            <div class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    {{ $error }}<br />
                                @endforeach
                            </div>
                        @endif


                            @if($reservation->data->timestamps->terminated_at == null && \Carbon\Carbon::parse($reservation->data->timestamps->expires_at) >= now())
                                <div class="font-size-h4 mb-4 text-center">
                                    Your lab reservation will expire in {{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->format('D') }} at {{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->format('g:i A T') }}<br />
                                    <span class="text-success">({{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->diffForHumans() }})</span><br />


                                        {{--<pacman-loader></pacmac-loader>--}}
                                        {{--<bar-loader class="custom-class" :color="#bada55" :loading="loading" :size="150" :sizeUnit="px"></bar-loader>--}}
                                        {{--<bar-loader class="custom-class" :loading="loading" :size="150" :sizeUnit="px"></bar-loader>--}}

                                    <div class="text-muted small text-center mt-3">Please allow up to 90 seconds for your lab environment to be provisioned.</div>

                                </div>
                            @elseif($reservation->data->timestamps->terminated_at != null)
                                <div class="font-size-h4 mb-4 text-center">
                                    Your lab reservation was terminated {{ \Carbon\Carbon::parse($reservation->data->timestamps->terminated_at)->format('M j g:i A T') }}<br />
                                    <span class="text-danger">({{ \Carbon\Carbon::parse($reservation->data->timestamps->terminated_at)->diffForHumans() }})</span><br />
                                </div>
                            @elseif($reservation->data->timestamps->terminated_at == null && \Carbon\Carbon::parse($reservation->data->timestamps->expires_at) < now())
                                <div class="font-size-h4 mb-4 text-center">
                                    Your lab reservation expired {{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->format('M j g:i A T') }}<br />
                                    <span class="text-danger">({{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->diffForHumans() }})</span><br />
                                </div>
                            @endif


                        <div class="block block-bordered block-rounded mt-5 mb-5">
                            <div class="block-content">
                                <table class="table table-condensed table-borderless">
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $reservation->data->relationships->tenant_user->data->relationships->auth_user_account->data->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email Address</th>
                                        <td>{{ $reservation->data->relationships->tenant_user->data->relationships->auth_user_account->data->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Start Time</th>
                                        <td>{{ \Carbon\Carbon::parse($reservation->data->timestamps->created_at)->format('D, M j g:i A T') }}</td>
                                    </tr>
                                    <tr>
                                        <th>End Time</th>
                                        <td>{{ \Carbon\Carbon::parse($reservation->data->timestamps->expires_at)->format('D, M j g:i A T') }}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        @if($reservation->data->tenant_reservation_token_id == null)
                            <div class="text-center">
                                <a class="btn btn-outline-primary btn-lg" href="{{ config('trylabs.portal.tenant.url') }}/user/reservations"><i class="fa fa-arrow-left mr-2"></i>Back to TryLabs Portal</a>
                            </div>
                        @endif

                    </div>

                    <!-- Footer -->
                    <div class="block-content bg-body-light border-top py-3">
                        <div class="row">
                            <div class="col-7">
                                @include('_partials.footer')
                            </div>
                            <div class="col-5 text-right">
                                @if($reservation->data->tenant_reservation_token_id != null)
                                    <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:{{ $reservation->data->relationships->cloud_image_template->data->relationships->tenant_account->data->user_support_email}}?s=Question about TryLabs Reservation {{ $reservation->data->short_id }} - {{ $reservation->data->relationships->tenant_reservation_token->data->name }}"><i class="fa fa-question-circle mr-2"></i>Contact the Lab Creator</a>
                                @else
                                    <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:support@trylabs.cloud?s=Question about Reservation {{ $reservation->data->short_id }} - {{ $reservation->data->relationships->tenant_user->data->relationships->auth_user_account->data->email }}"><i class="fa fa-question-circle mr-2"></i>Contact Lab Support</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_after')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-2012-2022.js"></script>
    <script>
        var timezone = moment.tz.guess();
        $('#timezone').val(timezone);
    </script>
@endsection
