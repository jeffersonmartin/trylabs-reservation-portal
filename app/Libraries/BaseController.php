<?php

namespace App\Libraries;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as LaravelController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Libraries\Traits\CloudstrationLabApiCalls;
use GuzzleHttp\Client;

class BaseController extends LaravelController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CloudstrationLabApiCalls;

    protected $base_view_path = 'admin.path.to.folder';
    protected $base_route_path = 'admin.placeholder';
    protected $dashboard_route = 'admin.dashboard.index';

    /**
     *   Returns the base view path for the resource
     *
     *   @return string     Ex. admin.path.to.folder
     */
    protected function baseViewPath()
    {
        return $this->base_view_path;
    }

    /**
     *   Returns the base route path for the resource
     *
     *   @return string     Ex. admin.placeholder
     */
    protected function baseRoutePath()
    {
        return $this->base_route_path;
    }

    /**
     *   Returns the dashboard route that is used for fallback if redirect_url is not set in a delete request.
     *
     *   @return string     Ex. admin.billing.dashboard.index
     */
    protected function dashboardRoute()
    {
        return $this->dashboard_route;
    }

}
