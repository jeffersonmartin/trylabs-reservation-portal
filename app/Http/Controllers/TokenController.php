<?php

namespace App\Http\Controllers;

use App\Models;
use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Auth\AuthUserAccount;
use Validator;

class TokenController extends BaseController
{

    public function index(Request $request)
    {

        abort(404);

        /*
        return view('token.index', compact([
            'request'
        ]));
        */

    }

    public function show($token_short_id, Request $request)
    {

        // TODO check if reservation exists already

        $reservation_token = Models\Tenant\TenantReservationToken::where('id', 'LIKE', $token_short_id.'%')->firstOrFail();

        $token = $this->getApiRequest('/v1/admin/tenant/reservation/tokens/'.$reservation_token->id);

        return view('token.show', compact([
            'request',
            'token'
        ]));

    }

    /**
     *   Redeem a Reservation Token
     *
     *   @param  Request $request [description]
     *
     *   @return [type]           [description]
     */
    public function redeem(Request $request)
    {

        // Get reservation token by id or abort with 404
        $tenant_reservation_token = $this->getApiRequest('/v1/admin/tenant/reservation/tokens/'.$request->tenant_reservation_token_id);

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'tenant_reservation_token_id' => 'required|uuid',
            'timezone' => 'required|timezone',
            'first_name' => 'required|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'last_name' => 'required|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'organization_name' => 'nullable|string|max:50',
            'email' => 'required|email|max:100',
            'password' => 'nullable|min:4|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{4,}$/',
            'flag_terms_accepted' => 'accepted',
            'flag_privacy_accepted' => 'accepted',
        ],
        [
            'password.min' => 'The password is at least 4 characters.',
            'password.regex' => 'Your password must contain an upper case letter, lowercase letter, number and one of the following symbols (#?!@$%^&*-).',
            'flag_terms_accepted.accepted' => 'You have not accepted the terms of service.',
            'flag_privacy_accepted.accepted' => 'You have not accepted the privacy policy.'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('token.show', ['token_short_id' => $tenant_reservation_token->data->short_id])
                ->withErrors($validator)
                ->withInput();
        }

        // Check if Password Protected
        if($tenant_reservation_token->data->flags->flag_is_password_protected == 1) {

            // If token password does not match provided password
            if($tenant_reservation_token->data->password != $request->password) {

                // Flash error message to session
                $request->session()->flash('error', 'The password provided is incorrect. Your reservation has not been created.');

                // TODO add info logging about failed redemptions

                // Redirect to show token page
                return redirect()->route('token.show');

            }

        }

        // Check if Redeem Count Protected
        if($tenant_reservation_token->data->flags->flag_is_redeem_count_protected == 1) {

            if($tenant_reservation_token->data->redeem_count >= $tenant_reservation_token->data->redeem_count_limit) {

                // Flash error message to session
                $request->session()->flash('error', 'This lab has reached the maximum number of redemptions allowed. Your reservation has not been created. Please contact the lab creator for assistance.');

                // TODO add info logging about failed redemptions

                // Redirect to show token page
                return redirect()->route('token.show');

            }

        }

        // Check if Rate Limit Protected
        if($tenant_reservation_token->data->flags->flag_is_rate_limit_protected == 1) {

            if($tenant_reservation_token->data->rate_limit_count >= $tenant_reservation_token->data->rate_limit_max) {

                if($tenant_reservation_token->data->rate_limit_interval == 'minute') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again in '.now()->diffInMinutes(\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)).' minutes. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'hour') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again in '.now()->diffInHours(\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)).' hours. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'day') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again in '.now()->diffInDays(\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)).' days. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'week') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again in '.now()->diffInDays(\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)).' days. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'month') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again after '.\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)->toDayDateTimeString().'. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'quarter') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again after '.\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)->toDayDateTimeString().'. Your reservation has not been created.';
                } elseif($tenant_reservation_token->data->rate_limit_interval == 'year') {
                    $error_message = 'This reservation token has reached the rate limit for this '.$tenant_reservation_token->data->rate_limit_interval.'. Please try again after '.\Carbon\Carbon::parse($tenant_reservation_token->data->rate_limit_resets_at)->toDayDateTimeString().'. Your reservation has not been created.';
                }

                // Flash error message to session
                $request->session()->flash('error', $error_message);

                // TODO add info logging about failed redemptions

                // Redirect to show token page
                return redirect()->route('token.show');

            }

        }

        // Create Tenant Reservation that will include the creation of user
        // and tenant account if the email address is new to our database
        $tenant_reservation = $this->postApiRequest('/v1/admin/tenant/reservations', [
            'tenant_reservation_token_id' => $tenant_reservation_token->data->id,
            'tenant_user_id' => $tenant_user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'organization_name' => $request->organization_name,
            'email' => $request->email,
            'timezone' => $request->timezone,
            'password' => array_get($request->all(), 'password')
        ]);

        return redirect()->route('reservation.show', ['reservation_short_id' => $tenant_reservation->data->short_id]);

    }

}
