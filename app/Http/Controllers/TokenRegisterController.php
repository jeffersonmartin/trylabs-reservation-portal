<?php

namespace App\Http\Controllers;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Auth\AuthUserAccount;
use Validator;

class TokenRegisterController extends BaseController
{

    public function create(Request $request)
    {

        // TODO check if reservation exists already

        return view('token.registration.create', compact([
            'request'
        ]));

    }

    public function store(Request $request)
    {

        /*
        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'timezone' => 'required|timezone',
            'email' => 'required|email|max:100',
            'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        ],
        [
            'password.min' => 'Your password is at least 8 characters.',
            'password.regex' => 'Your password contains an upper case letter, lowercase letter, number and one of the following symbols (#?!@$%^&*-).',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('auth.login.create')
                ->withErrors($validator)
                ->withInput();
        }

        // If timezone variable is set, then add timezone as session variable
        session(['timezone' => $request->timezone]);

        // Create array with credentials to check. Note that we can add additional
        // fields to check in the auth_user_accounts table if needed (useful for
        // active flags on normal log in, etc).
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'hashed_password' => Hash::make($request->password)
        ];

        // Use Laravel facade to attempt to log in
        if(Auth::attempt($credentials, $remember = true)) {

            // Generate a new authenticated session
            $request->session()->regenerate();

            // Redirect the user to the page previously attempting to access, or
            // to the dashboard if not specified
            return redirect()->intended(route('user.dashboard.index'));

        // If authentication failed, redirect an error message to the user
        } else {

            // Flash a message to the login page
            $user_check = AuthUserAccount::where('email', $request->email)->first();

            // If email account is not found in users table
            if(!$user_check) {
                return redirect()->route('auth.login.create')->with('alert', 'There is not an account with that email address. Would you like to create a new account?');
            }

            // If password does not match
            elseif(Hash::check($request->password, $user_check->password) == false) {
                return redirect()->route('auth.login.create')->with('alert', 'We found your account, however your password does not match our records. Please try again or reset your password.');
            }

            // TODO Check if account is locked, etc.

            // If no conditions match, redirect back to the login page
            else {
                return redirect()->route('auth.login.create');
            }

        }
        */

    }

}
