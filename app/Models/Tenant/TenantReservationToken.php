<?php

namespace App\Models\Tenant;

use App\Libraries;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cloud as CloudModels;

class TenantReservationToken extends Libraries\BaseModel
{
    use SoftDeletes;

    public $table = 'tenant_reservation_tokens';

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //
    // The created_at, updated_at, deleted_at fields should be in the array by
    // default. If there are additional date columns, add them to the array.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'starts_at',
        'expires_at',
        'rate_limit_resets_at',
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'tenant_account_id',
        'tenant_group_id',
        'tenant_user_id',
        'tenant_reservation_category_id',
        'cloud_image_template_id',
        'name',
        'slug',
        'external_notes',
        'internal_notes',
        'flag_is_redeem_count_protected',
        'redeem_count',
        'redeem_count_max',
        'flag_is_password_protected',
        'password',
        'flag_is_rate_limit_protected',
        'rate_limit_count',
        'rate_limit_count_max',
        'rate_limit_interval',
        'rate_limit_resets_at',
        'flag_is_time_limit_protected',
        'time_limit_minutes',
        'flag_is_public',
        'starts_at',
        'expires_at',
        'status'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response.
    //

    protected $casts = [
        'id' => 'string',
        'tenant_account_id' => 'string',
        'tenant_group_id' => 'string',
        'tenant_user_id' => 'string',
        'tenant_reservation_category_id' => 'string',
        'cloud_image_template_id' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'external_notes' => 'string',
        'internal_notes' => 'string',
        'flag_is_redeem_count_protected' => 'integer',
        'redeem_count' => 'integer',
        'redeem_count_max' => 'integer',
        'flag_is_password_protected' => 'integer',
        'password' => 'string',
        'flag_is_rate_limit_protected' => 'integer',
        'rate_limit_count' => 'integer',
        'rate_limit_count_max' => 'integer',
        'rate_limit_interval' => 'string',
        'rate_limit_resets_at' => 'datetime',
        'flag_is_time_limit_protected' => 'integer',
        'time_limit_minutes' => 'integer',
        'flag_is_public' => 'boolean',
        'starts_at' => 'datetime',
        'expires_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'status' => 'string'
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    /*
    public function tenantAccount() {
        return $this->belongsTo(TenantAccount::class, 'tenant_account_id');
    }

    public function tenantGroup() {
        return $this->belongsTo(TenantGroup::class, 'tenant_group_id');
    }

    public function tenantUser() {
        return $this->belongsTo(TenantUser::class, 'tenant_user_id');
    }

    public function tenantReservationCategory() {
        return $this->belongsTo(TenantReservationCategory::class, 'tenant_reservation_category_id');
    }

    public function cloudImageTemplate() {
        return $this->belongsTo(CloudModels\CloudImageTemplate::class, 'cloud_image_template_id');
    }
    */

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    /*
    public function tenantReservations() {
        return $this->hasMany(TenantReservation::class, 'tenant_reservation_token_id');
    }
    */

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //      DistantModel::class, IntermediateModel::class,
    //      'intermediate_key_id', 'distant_key_id', 'id'
    //      );
    //

    /*
    public function cloudInstances() {
        return $this->hasManyThrough(
            CloudModels\CloudInstance::class, TenantReservation::class,
            'tenant_reservation_token_id', 'tenant_reservation_id', 'id'
            );
    }

    public function cloudInstanceCycles() {
        return $this->hasManyThrough(
            CloudModels\CloudInstanceCycle::class, TenantReservation::class,
            'tenant_reservation_token_id', 'tenant_reservation_id', 'id'
            );
    }
    */

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(ForeignModel::class, 'package_this_foreign_table', 'this_table_id', 'foreign_table_id');
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

}
