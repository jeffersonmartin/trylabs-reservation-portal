<?php

namespace App\Models\Tenant;

use App\Libraries;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cloud as CloudModels;

class TenantReservation extends Libraries\BaseModel
{
    use SoftDeletes;

    public $table = 'tenant_reservations';

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //
    // The created_at, updated_at, deleted_at fields should be in the array by
    // default. If there are additional date columns, add them to the array.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'expires_at',
        'terminated_at'
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'tenant_account_id',
        'tenant_user_id',
        'tenant_reservation_category_id',
        'tenant_reservation_token_id',
        'cloud_image_template_id',
        'cloud_instance_id',
        'count_mins',
        'billable_mins',
        'terminate_action',
        'expires_at',
        'terminated_at',
        'status'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response.
    //

    protected $casts = [
        'id' => 'string',
        'tenant_account_id' => 'string',
        'tenant_user_id' => 'string',
        'tenant_reservation_category_id' => 'string',
        'tenant_reservation_token_id' => 'string',
        'cloud_image_template_id' => 'string',
        'cloud_instance_id' => 'string',
        'count_mins' => 'integer',
        'billable_mins' => 'integer',
        'terminate_action' => 'string',
        'expires_at' => 'datetime',
        'terminated_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'status' => 'string'
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    /*
    public function tenantAccount() {
        return $this->belongsTo(TenantAccount::class, 'tenant_account_id');
    }

    public function tenantUser() {
        return $this->belongsTo(TenantUser::class, 'tenant_user_id');
    }

    public function tenantReservationCategory() {
        return $this->belongsTo(TenantReservationCategory::class, 'tenant_reservation_category_id');
    }

    public function tenantReservationToken() {
        return $this->belongsTo(TenantReservationToken::class, 'tenant_reservation_token_id');
    }

    public function cloudImageTemplate() {
        return $this->belongsTo(CloudModels\CloudImageTemplate::class, 'cloud_image_template_id');
    }

    public function cloudInstance() {
        return $this->belongsTo(CloudModels\CloudInstance::class, 'cloud_instance_id');
    }
    */

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    /*
    public function cloudInstanceCycles() {
        return $this->hasMany(CloudModels\CloudInstanceCycle::class, 'tenant_reservation_id');
    }
    */

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //      DistantModel::class, IntermediateModel::class,
    //      'intermediate_key_id', 'distant_key_id', 'id'
    //      );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(ForeignModel::class, 'package_this_foreign_table', 'this_table_id', 'foreign_table_id');
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

}
