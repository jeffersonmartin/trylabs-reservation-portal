<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::middleware(['throttle:30,1'])->group(function() {

    //
    // Tenant Reservation Tokens
    //

    // Redirect the / path to /t/
    Route::get('/', function () {
        return redirect()->route('token.index');
    });

    // Landing page for root path where token is not included in the URL
    Route::get('/t', ['as' => 'token.index', 'uses' => 'TokenController@index']);

    // Landing page for token redemption page
    Route::get('/t/{token_short_id}', ['as' => 'token.show', 'uses' => 'TokenController@show']);

    // Redeem token form post
    Route::post('/t/{token_short_id}', ['as' => 'token.redeem', 'uses' => 'TokenController@redeem']);

    //
    // Registration Form
    //

    // User registration page for token redemption
    Route::get('/t/{token_short_id}/register', ['as' => 'token.register.create', 'uses' => 'TokenRegisterController@create']);

    // User registration form post
    Route::post('/t/{token_short_id}/register', ['as' => 'token.register.store', 'uses' => 'TokenRegisterController@store']);

    //
    // Tenant Reservations
    //

    // Landing page for root path where reservation is not included in the URL
    // We will use the session key set during redemption to redirect them
    Route::get('/r', ['as' => 'reservation.index', 'uses' => 'ReservationController@index']);

    // Landing page for token redemption page
    Route::get('/r/{reservation_short_id}', ['as' => 'reservation.show', 'uses' => 'ReservationController@show']);

    // TODO Payment Page for Reservation
    // TODO Extend reservation
    // TODO Terminate reservation

});

Route::namespace('Legal')->group(function () {

    Route::get('/legal', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/folders/44001193401');
    })->name('legal.terms.show');

    Route::get('/legal/terms/show', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781197');
    })->name('legal.terms.show');

    Route::get('/legal/privacy/show', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781198');
    })->name('legal.privacy.show');

});

Route::namespace('Support')->group(function () {

    Route::get('/support/kb', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/44000802406');
    })->name('support.kb.index');

    // Billing

    Route::get('/support/kb/trylabs/billing/cancellation-policy', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.billing.cancellation-policy');

    Route::get('/support/kb/trylabs/billing/plan-changes', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781194');
    })->name('support.kb.trylabs.billing.plan-changes');

    // Image Templates

    Route::get('/support/kb/trylabs/templates/how-to-build', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783483');
    })->name('support.kb.trylabs.templates.how-to-build');

    Route::get('/support/kb/trylabs/templates/best-practices', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783484');
    })->name('support.kb.trylabs.templates.best-practices');

    Route::get('/support/kb/trylabs/templates/tutorials-linux-ocean', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783490');
    })->name('support.kb.trylabs.templates.tutorials-linux-ocean');

    // Reservations

    Route::get('/support/kb/trylabs/reservations/ssh-keys', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781199');
    })->name('support.kb.trylabs.reservations.ssh-keys');

    Route::get('/support/kb/trylabs/reservations/using-guacamole-vnc', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783472');
    })->name('support.kb.trylabs.reservations.using-guacamole-vnc');

    Route::get('/support/kb/trylabs/reservations/using-guacamole-ssh', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783473');
    })->name('support.kb.trylabs.reservations.using-guacamole-ssh');

    // Lab Access Tokens

    Route::get('/support/kb/trylabs/tokens/how-to-redeem', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783485');
    })->name('support.kb.trylabs.tokens.how-to-redeem');

    Route::get('/support/kb/trylabs/tokens/best-practices', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783474');
    })->name('support.kb.trylabs.tokens.best-practices');

    Route::get('/support/kb/trylabs/tokens/ux', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783475');
    })->name('support.kb.trylabs.tokens.ux');

    Route::get('/support/kb/trylabs/tokens/share-email', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783476');
    })->name('support.kb.trylabs.tokens.share-email');

    Route::get('/support/kb/trylabs/tokens/share-sharepoint', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783477');
    })->name('support.kb.trylabs.tokens.share-sharepoint');

    Route::get('/support/kb/trylabs/tokens/share-html', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783478');
    })->name('support.kb.trylabs.tokens.share-html');

    Route::get('/support/kb/trylabs/tokens/share-youtube', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783479');
    })->name('support.kb.trylabs.tokens.share-youtube');

    Route::get('/support/kb/trylabs/tokens/share-lms-skilljar', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783480');
    })->name('support.kb.trylabs.tokens.share-lms-skilljar');

    Route::get('/support/kb/trylabs/tokens/share-lms-other', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783481');
    })->name('support.kb.trylabs.tokens.share-lms-other');

    // Legal

    Route::get('/support/kb/trylabs/legal/terms-of-service', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.legal.terms-of-service');

    Route::get('/support/kb/trylabs/legal/privacy-policy', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.legal.privacy-policy');

    /*
    Route::get('/support/kb/trylabs/roadmap/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.roadmap.xxx');
    */

    /*
    Route::get('/support/kb/trylabs/bugs/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.bugs.xxx');
    */

    /*
    Route::get('/support/kb/trylabs/releases/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.releases.xxx');
    */


});
